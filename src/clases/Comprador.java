package clases;

public class Comprador {

	String nombre;
	String obraComprada;
	int edad;
	String DNI;
	
	
	public Comprador(String nombre, int edad, String DNI) {
		
		this.nombre = nombre;
		this.edad = edad;
		this.DNI = DNI;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getObrasCompradas() {
		return obraComprada;
	}

	public void setObrasCompradas(String obrasCompradas) {
		this.obraComprada = obrasCompradas;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String toString() {
		return "El nombre del comprador es " + this.nombre + " tiene " + this.edad + " a�os " + " con DNI " + this.DNI
	+" las obras que ha comprado son "+ this.obraComprada ;
	}
}
