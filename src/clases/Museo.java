package clases;

import java.util.Scanner;

public class Museo {
	
	Cuadro[] cuadro;
	Comprador[] comprador;
	
	public Museo(int cantidadCuadros, int cantidadCompradores) {
		
		this.cuadro = new Cuadro[cantidadCuadros];
		this.comprador = new Comprador[cantidadCompradores];
	}
	
	public Comprador[] altaComprador(String nombre, int edad, String DNI) {
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] == null) {
				comprador[i] = new Comprador(nombre,edad,DNI);
				
				return comprador;
			}
		}
		return null;
	}
	public void listarGeneralComprador() {
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] != null) {
				
				System.out.println(comprador[i].toString());
			}
		}
	}
	
	public Cuadro[] altaCuadro(String autor, String titulo, String color, double altura, double ancho, double precio) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] == null) {
				cuadro[i] = new Cuadro(autor,titulo,color,altura, ancho, precio);
				
				return cuadro;
			}
		}
		return null;
	}
	
	public Cuadro buscar(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
					
					if(cuadro[i] != null) {
						
						if(titulo.equals(cuadro[i].getTitulo())) {
							
							return cuadro[i];
						}	
					}
				}
				return null;
			}
	
	public void eliminar(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					cuadro[i] = null;
				}
			}
		}
		
	}
	
	public void listarGeneralCuadro() {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				System.out.println(cuadro[i].toString());
			}
		}
	}
	
	
	public void cambiar(String tituloCambiar, String autor, String titulo, String color, double altura, double ancho, double precio) {
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					cuadro[i].setAutor(autor);
					cuadro[i].setTitulo(tituloCambiar);
					cuadro[i].setColor(color);
					cuadro[i].setAltura(altura);
					cuadro[i].setAncho(ancho);
					cuadro[i].setPrecio(precio);
					System.out.println("Cambios realizados con exito!!");
					System.out.println(cuadro[i].toString());
				}

			}
		}
	}
	
	
	public void listarAtributo(String titulo) {
		
		for (int i = 0; i < cuadro.length; i++) {
				
			if(cuadro[i] != null) {
					
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					System.out.println(cuadro[i]);
				}
				
			}
		}
	}
	
	public void dimension(String titulo) {
		
		double dimension = 0;
	
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(titulo.equals(cuadro[i].getTitulo())) {
					
					dimension = cuadro[i].getAltura()*cuadro[i].getAncho();
					System.out.println("La dimensión del cuadro " + cuadro[i].getTitulo()+ " es " + dimension + " cm2");
				}
			}
		}
	}
	
	public void comprarCuadro() {
		int poscionComprador = 0;
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Elige el nombre del comprador que va a realizar la compra");
		listarGeneralComprador();
		String nombreComprador = in.nextLine(); 
		System.out.println("Escribe uno de los siguientes nombres de cuadros para comprarlo");
		listarGeneralCuadro();
		
	
		String cuadrosParaComprar = in.nextLine();
		
		
		for (int i = 0; i < comprador.length; i++) {
			
			if(comprador[i] != null) {
				
				if(nombreComprador.equalsIgnoreCase(comprador[i].getNombre())) {
					poscionComprador = i;
				}
			}
			
		}
		
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				if(cuadrosParaComprar.equalsIgnoreCase(cuadro[i].getTitulo())) {
					
					comprador[poscionComprador].setObrasCompradas(cuadrosParaComprar);
				}
				
			}
		}
	}
	
	public void totalPrecioObrasMuseo() {
		double precioTotalObras = 0;
		for (int i = 0; i < cuadro.length; i++) {
			
			if(cuadro[i] != null) {
				
				precioTotalObras += cuadro[i].getPrecio();
				
			}
		}
		System.out.println("El precio total de las obras del museo es " + precioTotalObras);
	}
}
