package clases;

public class Cuadro {

	private String autor;
	private String titulo;
	private String color;
	private double altura;
	private	double ancho;
	private double precio;
	
	public Cuadro(String autor, String titulo, String color, double altura, double ancho, double precio) {
		this.autor = autor;
		this.titulo = titulo;
		this.color = color;
		this.altura = altura;
		this.ancho  = ancho;
		this.precio = precio;
	}
	/*
	 * @override
	 * */
	public String toString() {
		
		return "El autor de la obra es " + this.autor + " el titulo es "+
		this.titulo + " el color es " + this.color + ", su altura es "
		+ this.altura + ", la anchura es " + this.ancho + " y su precio es " + this.precio + "�";
	}
	
	public Cuadro() {
		
	}

	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public double getAltura() {
		return altura;
	}

	public void setAltura(double altura) {
		this.altura = altura;
	}

	public double getAncho() {
		return ancho;
	}

	public void setAncho(double ancho) {
		this.ancho = ancho;
	}
	
}
