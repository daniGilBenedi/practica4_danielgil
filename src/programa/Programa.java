package programa;


import java.util.Scanner;

import clases.Museo;

public class Programa {

	public static void main(String[] args) {
		
		Scanner in = new Scanner(System.in);
		int condicion = 0;
		System.out.println("�Cuantos cuadros va a tener el museo en total?");
		
		int numCuadros = in.nextInt();
		System.out.println("�Cuantos compradores va a tener el museo en total?");
		
		int numCompradores = in.nextInt();
		
		Museo museoDelPrado = new Museo(numCuadros, numCompradores);
		
		do {
			System.out.println("Elige una de las siguientes opciones: ");
			System.out.println("1. Dar de alta un cuadro");
			System.out.println("2. Buscar cuadro por nombre");
			System.out.println("3. Eliminar cuadro");
			System.out.println("4. Listar contenido del museo");
			System.out.println("5. Cambiar atributos de un cuadro");
			System.out.println("6. Listar contenido del museo por nombre");
			System.out.println("7. Dar de alta un comprador");
			System.out.println("8. Mostrar compradores");
			System.out.println("9. Comprar cuadro");
			System.out.println("10. Calcular el precio total de las obras del museo");
			System.out.println("11. Calcular la dimension del cuadro");
			System.out.println("12. Salir de la aplicaci�n");
			
			condicion = in.nextInt();
			in.nextLine();
			switch (condicion) {
			
			case 1:
				System.out.println("�Cuantos cuadros quieres a�adir al museo?");
				int numCuadrosAlta = in.nextInt();
				in.nextLine();
				int totalCuadros = 1;
				for (int i = 0; i < numCuadrosAlta; i++) {
					
					if(numCuadrosAlta <= numCuadros) {
					System.out.println("Rellena los campos del cuadro numero " + totalCuadros);
					System.out.println("Ingrese el autor");
					String autor = in.nextLine();
					System.out.println("Ingrese el titulo");
					String titulo = in.nextLine();
					System.out.println("Ingrese el color");
					String color = in.nextLine();
					System.out.println("Ingrese la altura");
					double altura = in.nextDouble();
					System.out.println("Ingrese el ancho");
					double ancho = in.nextDouble();
					System.out.println("Ingrese el precio");
					double precio = in.nextDouble();
					in.nextLine();
					museoDelPrado.altaCuadro(autor, titulo, color, altura, ancho, precio);
					totalCuadros++;
					}else {
						System.out.println("No se pueden ingresar mas cuadros del total");
					}
				}
			
				break;
			case 2:
				System.out.println("Introduce un titulo para ver si tu cuadro se encuentra en el museo");
				museoDelPrado.listarGeneralCuadro();
				String tituloBuscar = in.nextLine();
				System.out.println(museoDelPrado.buscar(tituloBuscar));
				break;
			case 3:
				System.out.println("Introduce un titulo para eliminar ese cuadro del museo");
				String tituloEliminar = in.nextLine();
				museoDelPrado.eliminar(tituloEliminar);
				break;
			case 4:
				museoDelPrado.listarGeneralCuadro();
				break;
			case 5:
				System.out.println("Ingresa el titulo del cuadro que desea cambiar");
				String titulo = in.nextLine();
				System.out.println("Ingresa los nuevos datos del cuadro");
				System.out.println("Ingrese el autor");
				String autor = in.nextLine();
				System.out.println("Ingrese el titulo");
				String tituloCambiar = in.nextLine();
				System.out.println("Ingrese el color");
				String color = in.nextLine();
				System.out.println("Ingrese la altura");
				double altura = in.nextDouble();
				System.out.println("Ingrese el ancho");
				double ancho = in.nextDouble();
				System.out.println("Ingrese el precio");
				double precio = in.nextDouble();
				in.nextLine();
				museoDelPrado.cambiar(tituloCambiar, autor, titulo, color, altura, ancho, precio);
				break;
			case 6:
				System.out.println("Ingresa el titulo de un cuadro para buscar informaci�n sobre el");
				String tituloInfo = in.nextLine();
				System.out.println(museoDelPrado.buscar(tituloInfo));
				break;
			case 7:
				System.out.println("�Cuantos compradores va a tener el museo?");
				int numCompradoresAlta = in.nextInt();
				in.nextLine();
				int totalCompradores = 1;
				
				for (int i = 0; i < numCompradoresAlta; i++) {
					if(numCompradoresAlta <= numCompradores) {
						
						System.out.println("Ingresa los datos del comprador " + totalCompradores);
						System.out.println("Introduce el nombre");
						String nombre = in.nextLine();
						System.out.println("Introduce la edad");
						int edad = in.nextInt();
						in.nextLine();
						System.out.println("Introduce el DNI");
						String DNI = in.nextLine();
						
						
						museoDelPrado.altaComprador(nombre, edad, DNI);
						totalCompradores++;
					}else {
						System.out.println("No se pueden ingresar mas compradores que el total");
					}
				}
				break;
			case 8:
				System.out.println("Los compradores que tiene el museo son: ");
				museoDelPrado.listarGeneralComprador();
				break;
			case 9:
					museoDelPrado.comprarCuadro();
					System.out.println("La operaci�n se ha realizado con exito!!");
				break;
			case 10:
				museoDelPrado.totalPrecioObrasMuseo();
				break;
				
			case 11:
				System.out.println("Ingresa el titulo del cuadro del que quieras saber las dimensiones");
				String tituloDimension = in.nextLine();
				museoDelPrado.dimension(tituloDimension);
				break;
			
			case 12:
				System.out.println("Cerrando aplicaci�n...");
				break;
			default:
				System.out.println("Seleccione una opci�n correcta");
				break;
			}
			
			
		}while(condicion!=12);
		in.close();	
	}

}
